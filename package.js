Package.describe({
  name: 'apconic:migration',
  version: '1.0.0',
  summary: 'Migration tool for meteor app',
  git: 'https://gitlab.com/apconic-open/migration.git',
  documentation: 'README.md',
});

Npm.depends({
  lodash: '4.13.1',
  chai: '3.5.0',
  mocha: '2.5.3',
});

Package.onUse(function(api) {
  api.versionsFrom('1.4-rc.0');
  api.use('ecmascript');
  api.mainModule('migration.js');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('migration');
  api.mainModule('migration.specs.js');
});


Npm.depends({
  lodash: '4.13.1',
  chai: '3.5.0',
  mocha: '2.5.3',
});

Package.onUse(function(api) {
  api.versionsFrom('1.4-rc.0');
  api.use('ecmascript');
  api.mainModule('migration.js');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('migration');
  api.mainModule('migration.specs.js');
});
