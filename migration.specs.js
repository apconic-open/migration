const { describe, it } = global;
import { expect, assert } from 'chai';
import { migrate } from 'meteor/migration';

let versions = [];
const migrationConfig = {
  '1.11.1'() {
    versions.push({ version: '1.11.1' });
  },
  '2.1.3'() {
    versions.push({ version: '2.1.3' });
  },
  '2.8.1'() {
    versions.push({ version: '2.8.1' });
  },
  '2.1.1'() {
    versions.push({ version: '2.1.1' });
  },
  '1.0'() {
    versions.push({ version: '1.0' });
  },
  'asfasf'() {
    versions.push({ version: '1.0' });
  },
};

describe('test migration ', () => {
  beforeEach(() => {
    versions = [];
  });

  it('should throw error if config is null or invalid', () => {
    try {
      migrate(null, '1.9', '2.8');
      assert.isNotOk(true, 'should not come here');
    } catch (error) {
      assert.isDefined(error);
    }
    expect(versions.length).to.equal(0);
  });

  it('should not migrate all the versions if fromVersion and toVersion are not specified',
  () => {
    migrate(migrationConfig);
    expect(versions.length).to.equal(5);
  });

  it('should not migrate all the versions greater than fromVersion if toVersion is not specified',
  () => {
    migrate(migrationConfig, '1.1');
    expect(versions.length).to.equal(4);
  });

  it('should not migrate all the versions upto toVersion if fromVersion is not specified',
  () => {
    migrate(migrationConfig, null, '2.8');
    expect(versions.length).to.equal(4);
  });

  it('should migrate from and to specified versions successfully excluding fromVersion', () => {
    migrate(migrationConfig, '1.0', '2.8');
    expect(versions.length).to.equal(3);
    expect(versions[0].version).to.equal('1.11.1');
    expect(versions[1].version).to.equal('2.1.1');
    expect(versions[2].version).to.equal('2.1.3');
  });
});
