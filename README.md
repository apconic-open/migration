Import migrate function in your app as follows

import { migrate } from 'meteor/migration';

The 'migrate' function takes 3 params 'migrationConfig', 'fromVersion' and 'toVersion'.

1. Migration config - It contains functions with names of versions it upgrades to.

e.g.

const migrationConfig = {
  '1.1.0'() {
    // code to upgrade from pervious version(1.0.0) to 1.1.0
  },
}

NOTE: It is built according to semantic versioning scheme and supports only numbers in versions.
The functions names need not to be in the sequence.
It will run all the function given in the configuration from 'fromVersion' to 'toVersion' excluding 'fromVersion' function.

2. From veriosn - Last version of the software. Default value is 0

3. To version - Version to which the app needs to be upgraded.
if value is not provided it will upgrade to the latest version in the config.

NOTE: Call the migrate funcion in Meteor.startup()