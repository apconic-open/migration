import { keysIn, isNull, isObject } from 'lodash';

function _getHighLow(ver1, ver2) {
  const splittedVersion1 = ver1.split('.');
  const majorVersion1 = parseInt(splittedVersion1[0] || 0, 10);
  const minorVersion1 = parseInt(splittedVersion1[1] || 0, 10);
  const nanoVersion1 = parseInt(splittedVersion1[2] || 0, 10);
  const splittedVersion2 = ver2.split('.');
  const majorVersion2 = parseInt(splittedVersion2[0] || 0, 10);
  const minorVersion2 = parseInt(splittedVersion2[1] || 0, 10);
  const nanoVersion2 = parseInt(splittedVersion2[2] || 0, 10);
  if ((majorVersion1 > majorVersion2) ||
    ((majorVersion1 === majorVersion2) && (minorVersion1 > minorVersion2) ||
      (majorVersion1 === majorVersion2 && minorVersion1 === minorVersion2)
       && (nanoVersion1 > nanoVersion2)
    )) {
    return {
      low: ver2,
      high: ver1,
    };
  }
  return {
    low: ver1,
    high: ver2,
  };
}

function _getSortedVersions(config) {
  const versions = keysIn(config);
  for (let i = (versions.length - 1); i >= 0; i--) {
    for (let j = 1; j <= i; j++) {
      const minMax = _getHighLow(versions[j - 1], versions[j]);
      versions[j - 1] = minMax.low;
      versions[j] = minMax.high;
    }
  }
  return versions;
}

function _getVersionToUpgrade(versions, fromVersion = '0.0', toVersion = '99.0') {
  const versionsToUpgrade = [];
  versions.forEach((ver) => {
    const fromComparison = _getHighLow(fromVersion, ver);
    const toComparison = _getHighLow(toVersion, ver);
    if ((ver !== fromVersion) && (fromComparison.low === fromVersion) &&
      (toComparison.high === toVersion)) {
      versionsToUpgrade.push(ver);
    }
  });
  return versionsToUpgrade;
}

function migrate(config, fromVersion, toVersion) {
  if (!isObject(config)) {
    throw new Error('Invalid configuration');
  }
  const fromVer = isNull(fromVersion) ? '0.0' : fromVersion;
  const toVer = isNull(toVersion) ? '99.0' : toVersion;
  const versions = _getSortedVersions(config);
  const versionsToUpgrade = _getVersionToUpgrade(versions, fromVer, toVer);
  versionsToUpgrade.forEach((ver) => {
    config[ver]();
  });
}

export {
  migrate,
};
